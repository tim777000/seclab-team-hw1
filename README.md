# README
> b05902012    b05902078
> b05902090    b05902109

## Q1.
- **Vagrantfile** 用來對 VM 做設定的 config 檔，開發者可以透過 **Vagarntfile** 設定檔來達成自動化的動作。
- 舉例來說
    - 範例程式中
    ``` Ruby
    Vagrant.configure("2") do |config|
        config.vm.box = "bento/ubuntu-16.04"
        ...
        config.vm.provision "shell", path: "scripts/install_docker.sh", privileged: false
    end
    ```
    - 可以檢查是否已下載 bento/ubuntu-16.04 這個 box
    - 讓腳本 **scripts/install_docker.sh** 自動安裝
- 以上檢查 VM 中的環境參與、安裝版本等功能，與執行腳本的功能，都能使開發者在大量佈署時，減少負擔。

## Q2.
### 1. 從 Ubuntu 到 python:3.7-alpine
- *"Alpine Linux 是一套極小安全又簡單的作業系統，在現今 Docker Images 裡面，最主要推崇的就是 Ubuntu 作業系統，但是令人詬病的是 Ubuntu 還是不夠小"* -- 引述自網路
- 因此將 docker image 轉成 Alpine，開發者可以使用更小的機器來佈署服務。
- 後面使用 ```apk``` 取代 ```apt-get``` 就是為了這個系統轉移而更改的。

### 2. No Cache
```Dockerfile
RUN apk add --no-cache ...
```
- 安裝 package 時不使用 cache，而是重新下載。
- 若攻擊者有能力 access 到未佈署前的硬體，這個方法以 security 層面來說會比較安全，開發者將不會意外安裝到有問題的 package。

### 3. 更改相關檔案的權限
``` Dockerfile
RUN adduser -D -u 1001 -s /bin/sh ctfd
RUN chown -R 1001:1001 /opt/CTFd /var/log/CTFd /var/uploads
```
- 在舊版本中，完全沒有對 **/opt/CTFd**、**/var/log/CTFd**、**/var/uploads** 的權限進行修正。而新版本中，使用遞迴的方式將底下所有子檔案的擁有者都設為 1001 群組中的使用者 1001。
- 減少被有心人士 access 到的風險。

### 4. 移除 CMD
``` Ruby
ENTRYPOINT ["/opt/CTFd/docker-entrypoint.sh"]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "-w", ...]
```
- 在舊版本中，最後將服務 listen 在 0.0.0.0:8000 是 hardcode 寫在 **Dockerfile** 中，而在新版本中，是寫到 **/opt/CTFd/docker-entrypoint.sh** 最後面。
- 有以下幾點好處
    - 以 code style 來說，使用 CMD 會使的可讀性與版面變差
    - 以 security 來說，使用 bash 能夠用變數的方式替代有隱私疑慮的參數，例如 log 位置等。避免有心人從 source code 中得知重要資訊。

## Q3.
### Deploy Strategy A -- Git pull and upgrade code on server

- How it works

| Requirements                 | how we achieve |
| ---------------------------- | -------------- |
| Secure password              | GCP的service因為有service account的json金鑰檔，可以直接登入，再利用Environment variables達成Secure |
| Avoid hardcoding secrets     | 利用GitLab Project Setting裡CI/CD設定Environment variables的功能來防止 hardcoding secrets |
| Secure communications        | 使用ssh傳輸協定來溝通 |
| Access Control of CD trigger | 將某些Environment variables設為protected，讓只有特定protected的branches可以使用 |

- How to initialize
    - 使用 gitlab ci 進行 automation scripts，以下將概述已經自動化的流程，使用者僅需要提供 variable 與開好權限的 instance、service account。
    - 需要的 variable
        - ```$AWS_ACCESS_KEY_ID```
        - ```$AWS_SECRET_ACCESS_KEY```
        - ```$CODECOV_TOKEN```
        - ```$USER_A```: eg. username@device_name
        - ```$ZONE_A```: eg. asia-east1-a
        - ```$GCLOUD_CREDENTIALS_JSON_A```: gcp service account json file
    - ```gcloud auth activate-service-account``` 指令設定 service account 權限。```$ACCOUNT_JSON_A``` 會在此作為參數使用。
    - ```gcloud compute ssh``` 指令將會登入於 ```$ZONE_A``` 的 ```$USER_A```
    - rm -rf seclab-team-hw1/
    - sudo apt-get install git -yq
    - git clone https://gitlab.com/tim777000/seclab-team-hw1.git
    - sudo apt-get install python3-pip -yq
    - cd seclab-team-hw1/
    - sudo python3 -m pip install -r requirements.txt
    - sudo python3 -m pip install gunicorn
    - pkill -9 gunicorn
    - gunicorn --bind 0.0.0.0:8000 -w 4 "CTFd:create_app()"
    - exit
    - 依照上述指令依序執行可完成deploy，可自動執行但有高機率發生錯誤，目前尚未解決，手動依序執行可確保成功率，手動第一次若失敗再執行一次gunicorn --bind 0.0.0.0:8000 -w 4 "CTFd:create_app()"就會成功。


### Deploy Strategy B -- Docker deployment

- How it works

| Requirements                 | how we achieve |
| ---------------------------- | -------------- |
| Secure password              | 使用keyfile登陸GCP帳號
| Avoid hardcoding secrets     | 使用CI/CD variables |
| Secure communications        | 使用ssh, https傳輸協定來溝通|
| Access Control of CD trigger | 將某些Environment variables設為protected，讓只有特定protected的branches可以使用 |

- How to initialize
    - 使用 gitlab ci 進行 automation scripts，以下將概述已經自動化的流程，使用者僅需要提供 variable 與開好權限的 instance、service account。
    - 需要的 variable
        - ```$AWS_ACCESS_KEY_ID```
        - ```$AWS_SECRET_ACCESS_KEY```
        - ```$CODECOV_TOKEN```
        - ```$USER_B```: eg. username@device_name
        - ```$ZONE_B```: eg. asia-east1-a
        - ```$GCLOUD_CREDENTIALS_JSON_B```: gcp service account json file
    - 自行寫了install_something.sh 來install一些package
    - 使用sudo docker-compose up -d 來在docker deploy
    - 修改了docker-compose.yml將port 導到80 port